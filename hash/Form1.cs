﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace hash
{
    public partial class Form1 : Form
    {
        HashTable table = new HashTable();
        private string FileName = "";
        public Form1()
        {
            InitializeComponent();
        }

        //отображение таблицы
        public void TableDraw()
        {
            List<Film> list = table.TableToList();
            dataGridView1.Rows.Clear();
            dataGridView1.RowCount = list.Count;
            for (int i = 0; i < list.Count; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = list[i].title;
                dataGridView1.Rows[i].Cells[1].Value = list[i].director;
                string allActors = "";
                foreach (string s in list[i].actors)
                {
                    allActors += s + "\n";
                }
                dataGridView1.Rows[i].Cells[2].Value = allActors;
                dataGridView1.Rows[i].Cells[3].Value = list[i].summary;
            }
        }

        //выход
        private void выToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        //условие задачи
        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("6. Задан набор записей следующей структуры: название кинофильма, режиссер, список актеров, краткое содержание. По заданному названию найти остальные сведения. Метод разрешения коллизии: квадратичное опробование", "Условие задачи", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileName = "";
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Json File|*.json";
            if (openFileDialog1.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                FileName = openFileDialog1.FileName;
                List<Film> list = JsonConvert.DeserializeObject<List<Film>>((File.ReadAllText(FileName)));
                foreach (Film f in list)
                {
                    table.Add(f);
                }
                TableDraw();
            }
        }

        private void SaveFile(string fileName)
        {
            List<Film> list = table.TableToList();
            File.WriteAllText(fileName, JsonConvert.SerializeObject(list));
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveFile(FileName);
            }
            else
            {
                сохранитьКакToolStripMenuItem_Click(sender, e);
            }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Json File|*.json";
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                SaveFile(saveFileDialog1.FileName);
            }
        }

        private void добавитьФильмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FilmForm filmForm = new FilmForm(FormState.ADD);
            filmForm.ShowDialog();
            if (filmForm.isRightClosed)
            {
                if (!table.Add(filmForm.f))
                {
                    MessageBox.Show("Фильм уже записан", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    TableDraw();
                }
            }
        }

        private void редактироватьФильмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringForm strForm = new StringForm(FormState1.TITLE);
            strForm.ShowDialog();
            if (strForm.str != "")
            {
                Film tmp = table.Search(strForm.str);
                if (tmp != null)
                {
                    FilmForm filmForm = new FilmForm(FormState.EDIT, tmp);
                    filmForm.ShowDialog();
                    if (filmForm.isRightClosed)
                    {
                        table.Delete(tmp);
                        table.Add(filmForm.f);
                        TableDraw();
                    }
                }
                else
                {
                    MessageBox.Show("Фильм не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void удалитьФильмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringForm strForm = new StringForm(FormState1.TITLE);
            strForm.ShowDialog();
            if (strForm.str != "")
            {
                Film tmp = table.Search(strForm.str);
                if (tmp != null)
                {
                    table.Delete(tmp);
                    TableDraw();
                }
                else
                {
                    MessageBox.Show("Фильм не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void искатьФильмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringForm strForm = new StringForm(FormState1.TITLE);
            strForm.ShowDialog();
            if (strForm.str != "")
            {
                Film tmp = table.Search(strForm.str);
                if (tmp != null)
                {
                    FilmForm filmForm = new FilmForm(FormState.DISPLAY, tmp);
                    filmForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Фильм не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
