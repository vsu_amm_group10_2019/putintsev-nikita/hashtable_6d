﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hash
{
    class HashTable
    {
        struct Element
        {
            public Film film;
            public bool deleted;
        }
        Element[] table;
        const int c = 17, d = 11;

        //конструктор
        public HashTable(int size = 50)
        {
            table = new Element[size];
        }

        //хеш-функция
        public int GetHash(string title)
        {
            int result = 0;
            foreach (char c in title)
            {
                result += c;
            }
            return result;
        }

        //поиск по названию фильма
        public Film Search(string title)
        {
            int hash = GetHash(title), 
                i = 0,
                key = hash % table.Length;
            while (table[key].film != null || table[key].deleted)
            {
                if (table[key].film != null && table[key].film.title == title)
                {
                    return table[key].film;
                }
                else i++;
                key = (hash + c * i + d * i * i) % table.Length;
            }
            return null;
        }

        //добавление фильма
        public bool Add(Film f)
        {
            if (Search(f.title) != null)
            {
                return false;
            }
            else
            {
                int hash = GetHash(f.title),
                i = 0,
                key = hash % table.Length;
                while (i < table.Length && table[key].film != null)
                {
                    i++;
                    key = (hash + c * i + d * i * i) % table.Length;
                }
                if (table[key].film == null)
                {
                    table[key].film = f;
                    table[key].deleted = false;
                }
                return true;
            }
        }

        //удаление фильма
        public void Delete(Film f)
        {
            int hash = GetHash(f.title),
                i = 0,
                key = hash % table.Length;
            while (table[key].film != null || table[key].deleted)
            {
                if (table[key].film != null && table[key].film == f)
                {
                    table[key].film = null;
                    table[key].deleted = true;
                    return;
                }
                i++;
                key = (hash + c * i + d * i * i) % table.Length;
            };
        }

        //получение списка всех фильмов
        public List<Film> TableToList()
        {
            List<Film> list = new List<Film>();
            foreach (Element e in table)
            {
                if (e.film != null)
                {
                    list.Add(e.film);
                }
            }
            return list;
        }
    }
}