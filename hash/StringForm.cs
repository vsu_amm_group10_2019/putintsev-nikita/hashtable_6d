﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hash
{
    public partial class StringForm : Form
    {
        private FormState1 FormState;
        public string str = "";

        public StringForm(FormState1 formState)
        {
            InitializeComponent();
            FormState = formState;
            textBox1.Text = "";
            switch (formState)
            {
                case FormState1.TITLE:
                    label1.Text = "Введите название фильма:";
                    break;
                case FormState1.ACTOR:
                    label1.Text = "Введите имя актера:";
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            str = textBox1.Text;
            Close();
        }
    }
}
